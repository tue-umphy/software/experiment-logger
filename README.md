# ⏱️  Experiment Logger

Utility to generate a CSV logfile with start/end timestamps of a series of experiments using a (wireless) keypad ⌨

## 📥 Installation

```bash
# From Gitlab
pip install git+https://gitlab.com/tue-umphy/software/experiment-logger
```

## ❓ Usage

```bash
# 📟 From the Temrminal
experiment-logger

python -m experimentlogger  # 👈 If just 'experiment-logger' doesn't work

# ❓ Help
experiment-logger -h
```

## 📝 Output

TDB...

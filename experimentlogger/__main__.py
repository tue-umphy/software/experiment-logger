# system modules
import argparse
import collections
import csv
import datetime
import itertools
import json
import logging
import os
import re
import shlex
import socket
import sys
import types
import io

# external modules
import pyfiglet
import rich
from rich.align import Align
from rich.columns import Columns
from rich.console import Console
from rich.json import JSON
from rich.padding import Padding
from rich.panel import Panel
from rich.table import Table
from rich.syntax import Syntax
from rich.emoji import Emoji


def numsort(s):
    """
    Split a string at number boundaries and convert the numbers to actual
    numbers. Useful for 'numeric' string sorting with
    ``sorted(strings,key=numsort)``.
    """

    def str2num(s):
        try:
            return int(s)
            return float(s)
        except:
            pass
        return s

    pattern = re.compile(r"(\D+)|(\d+)")
    return tuple(
        str2num(x)
        for x in itertools.chain.from_iterable(pattern.findall(str(s)))
        if x
    )


def now():
    return datetime.datetime.utcnow().replace(tzinfo=datetime.timezone.utc)


parser = argparse.ArgumentParser(
    prog="python -m experimentlogger"
    if "__main__.py" in sys.argv[0]
    else sys.argv[0],
    description=r"""
 _____                      _                      _     _                                
| ____|_  ___ __   ___ _ __(_)_ __ ___   ___ _ __ | |_  | |    ___   __ _  __ _  ___ _ __ 
|  _| \ \/ / '_ \ / _ \ '__| | '_ ` _ \ / _ \ '_ \| __| | |   / _ \ / _` |/ _` |/ _ \ '__|
| |___ >  <| |_) |  __/ |  | | | | | | |  __/ | | | |_  | |__| (_) | (_| | (_| |  __/ |   
|_____/_/\_\ .__/ \___|_|  |_|_| |_| |_|\___|_| |_|\__| |_____\___/ \__, |\__, |\___|_|   
           |_|                                                      |___/ |___/           

⏱️ Experiment Logger

Utility to generate a CSV logfile with start/end timestamps of a series of experiments using a (wireless) keypad ⌨
""",
    formatter_class=argparse.RawDescriptionHelpFormatter,
)
parser.add_argument(
    "-o",
    "--output",
    help="output file path, auto-generated name in current directory if not specified",
    type=argparse.FileType("w"),
)
parser.add_argument(
    "-f",
    "--fields",
    help="Additional metadata fields that are asked and stored for every experiment.",
    default=("comment",),
    type=lambda x: tuple(s.strip() for s in re.split(r"\s*,\s*", x)),
)
parser.add_argument(
    "-v",
    "--verbose",
    help="verbose output",
    action="store_true",
)

logger = logging.getLogger("experiment-logger")


def linelength(text):
    return max(map(len, text.splitlines()))


def fits_screen(text):
    return all(
        a < b
        for a, b in zip(
            (linelength(text) + 4, len(text.splitlines()) + 5),
            os.get_terminal_size(),
        )
    )


def fitting_figlet(text, center=False):
    fonts = ("big", "standard", "small")
    # first try to fit a figlet into a single line
    for font in fonts:
        figlet = pyfiglet.Figlet(
            font=font, justify="left", width=10000
        ).renderText(text)
        if fits_screen(figlet):
            yield pyfiglet.Figlet(
                font=font,
                justify="center" if center else "left",
                width=os.get_terminal_size().columns,
            ).renderText(text)
    # if no font fits into a single line, just break lines
    for font in fonts:
        figlet = pyfiglet.Figlet(
            font=font,
            justify="center" if center else "left",
            width=os.get_terminal_size().columns,
        ).renderText(text)
        if fits_screen(figlet):
            yield figlet
    yield f" {text} ".center(
        os.get_terminal_size().columns
    ) if center else text


def figlet(text):
    return next(fitting_figlet(text))


def cli():
    args = parser.parse_args()
    logging.basicConfig(
        level=logging.DEBUG if args.verbose else logging.WARNING
    )
    if not (logfile := args.output):
        logfile = open(
            f"{now().strftime('%Y-%m-%dT%H.%M.%S%z')}-experiment-logger.csv",
            "w",
        )
    logfilewrite = logfile.write
    logfilewritten = io.StringIO()
    # patch logfile for logging
    def logwrite(self, x):
        # sys.stdout.write(f"📂 {x}")
        logfilewritten.write(x)
        rich.print(
            Align.center(
                Panel.fit(
                    Padding(
                        Syntax(
                            logfilewritten.getvalue(),
                            line_numbers=True,
                            theme="solarized",
                            lexer="python",
                        ),
                        (1, 1),
                    ),
                    title=Emoji.replace(f":open_file_folder: {logfile.name}"),
                )
            )
        )
        return logfilewrite(x)

    logfile.write = types.MethodType(logwrite, logfile)
    logger.info(
        f"Lines prefixed with the folder symbol (📂) indicate what's written to file {logfile.name!r}"
    )
    csvwriter = csv.DictWriter(
        logfile,
        fieldnames=("nr", "time_start", "time_end") + args.fields,
    )
    logfile.write(f"# file: {os.path.basename(logfile.name)}\r\n")
    logfile.write(
        f"# command-line: "
        f"{os.getlogin()}@{socket.gethostname()}> "
        f"{' '.join([parser.prog]+list(map(shlex.quote,sys.argv[1:])))}\r\n"
    )
    csvwriter.writeheader()
    logfile.flush()
    experiments = list()

    def experiments_to_table(experiments):
        numbers = tuple(filter(bool, (exp.get("nr") for exp in experiments)))
        table = Table(
            title=f"Experiment{' Nr. ' + str(numbers[0]) if len(numbers) == 1 else 's'}"
        )
        all_keys = set(
            itertools.chain.from_iterable(
                [exp.keys() for exp in experiments] + [args.fields]
            )
        )
        keys = [
            [k, all_keys.discard(k)][0]
            for k in ("nr", "time_start", "time_end")
        ] + sorted(all_keys)
        for col in keys:
            table.add_column(col, justify="right")

        for experiment in experiments:
            table.add_row(*(str(experiment.get(k, "")) for k in keys))

        return table

    for nr in itertools.count(1):
        experiments.append(experiment := dict(nr=nr))
        try:
            while True:
                rich.print(Align.center(experiments_to_table([experiment])))
                cmd = input(
                    f"📟 [ENTER]: {'stop' if experiment.get('time_start') else 'start'} experiment {experiment['nr']}\t"
                    f"[fieldname value ENTER] set field value"
                    f"\r\n",
                )
                if not cmd.strip():
                    if experiment.get("time_start"):
                        print(f"✅ finished experiment nr {experiment['nr']}")
                        experiment["time_end"] = now().strftime("%FT%T.%f%z")
                        csvwriter.writerow(
                            {
                                f: experiment.get(f, "")
                                for f in csvwriter.fieldnames
                            }
                        )
                        logfile.flush()
                        rich.print(
                            Align.center(
                                experiments_to_table(
                                    collections.deque(experiments, maxlen=5)
                                )
                            )
                        )
                        break
                    else:
                        print(f"⏱️  starting experiment nr {experiment['nr']}")
                        rich.print(
                            Align.center(
                                Panel(
                                    Padding(
                                        figlet(
                                            f"Experiment Nr. {experiment['nr']}"
                                        ),
                                        (0, 4),
                                    ),
                                    expand=False,
                                )
                            )
                        )
                        experiment["time_start"] = now().strftime("%FT%T.%f%z")
                        continue
                it = iter(re.split(r"\s+", cmd, maxsplit=1))
                field, value = (next(it, None) for i in range(2))
                logger.debug(f"{field = !r}    {value = !r}")
                m1 = [
                    f
                    for f in args.fields
                    if field.lower().startswith(f.lower())
                ]
                m2 = [f for f in args.fields if field.lower() in f.lower()]
                if len(m1) == 1:
                    experiment[(field := m1[0])] = value
                    print(f"Set {field} = {value!r}")
                    logger.info(f"{experiment = }")
                elif len(m2) == 1:
                    experiment[(field := m2[0])] = value
                    print(f"Set {field} = {value!r}")
                    logger.info(f"{experiment = }")
                else:
                    print(
                        f"Unclear which field you mean. Choose from {args.fields!r}"
                    )
        except KeyboardInterrupt:
            logger.info(f"🛑 You pressed CTRL-C. Stopping.")
            rich.print(
                Align.center(
                    experiments_to_table(
                        experiments[min(len(experiments), 0) :]
                    )
                )
            )
            break


if __name__ == "__main__":
    cli()
